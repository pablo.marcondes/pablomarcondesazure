from flask import Blueprint, request, flash, redirect, render_template, url_for, current_app, session, Response, abort
from flask.views import MethodView
from resourceprovider.models import Subscription, Event, CloudService, Resource
from resourceprovider import app, db
from datetime import datetime
from xmlbuilder import XMLBuilder
import untangle
import hashlib
import iso8601
from decorators import log_request_body

mod = Blueprint('general', __name__)
app.debug = True

sso_secret = 'some_massive_secret'

@app.route('/')
def index():
    return "<h1>This flask app is running!</h1>"

